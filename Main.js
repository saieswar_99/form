import React, { Component } from 'react'; 
import { Route, 
    NavLink, 
    HashRouter
} from 'react-router-dom';
import Create from './Create';
import View from './View';
import Contact from './Contact'; 
import "./index.css";

class Main extends Component {
    render(){
        return(
            <HashRouter>
            <div>
            <h1>Enter Your Information</h1>
            <ul className ="header">
                <li><NavLink exact to='/Create'>Create</NavLink></li>
                <li><NavLink to='/View'>View</NavLink></li>
                <li><NavLink to='/contact'>Contact</NavLink></li>
            </ul>
            <div className = "content">
                <Route exact path="/Create" component={Create}/>
                <Route path="/View" component={View}/>
                <Route path="/contact" component={Contact}/>
            </div>
        </div>
            </HashRouter>
        );
    }
}

export default Main; 
